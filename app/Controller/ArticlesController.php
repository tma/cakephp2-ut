<?php
class ArticlesController extends AppController {

	public $components = array(
        'Search.Prg',
        'Paginator'
    );

	public function index() {
        $this->Prg->commonProcess();
        $this->Paginator->settings['conditions'] = $this->Article->parseCriteria($this->Prg->parsedParams());
        $this->set('articles', $this->Paginator->paginate());
    }
}