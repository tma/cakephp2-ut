<?php
app::uses('AppController', 'Controller');

class FacilitiesController extends AppController 
{

	public function index() 
	{
		// limit = 1
		$facilities = $this->Facility->find('all', ['limit' => 1, 'fields' => ['id', 'facility_name', 'main_image']]);
		// limit = 2 and deleted = 1
		$facilities_deleted = $this->Facility->find('all', ['limit' => 2, 'fields' => ['id', 'facility_name', 'main_image', 'deleted'], 'conditions' => ['deleted' => 1]]);
		$this->set('facilities', $facilities);
		$this->set('facilities_deleted', $facilities_deleted);
		
	}

	public function add()
	{
		if (!empty($this->request->data)) {
			$this->Facility->save($this->request->data);
		}
	}
}