<?php
App::uses('Folder', 'Utility');
class HelloShell extends AppShell
{
	public function main()
	{
		$this->delTmpFolder('facility');
		$this->delTmpFolder('blog');
	}

	public function delTmpFolder($type)
	{
		switch ($type) {
			case 'facility':
				$tmpImgPath = Configure::read('Image.tmp.tmp.path');
				$tmpPdfPath = Configure::read('Pdf.tmp.tmp.path');
				break;
			
			default:
				$tmpImgPath = Configure::read('Image.tmp.tmp.blog.path');
				break;
		}
		if (!empty($tmpImgPath)) {
			$tmpImgPath = str_replace('%s' . DS, '', $tmpImgPath);
			$folder = new Folder(WWW_ROOT . $tmpImgPath);
			$subFolders = $folder->read()[0];
			$this->__delTmpSubFolder(WWW_ROOT . $tmpImgPath, $subFolders);
			unset($folder);
		}
		if (!empty($tmpPdfPath)) {
			$tmpPdfPath = str_replace('%s' . DS, '', $tmpPdfPath);
			$folder = new Folder(WWW_ROOT . $tmpPdfPath);
			$subFolders = $folder->read()[0];
			$this->__delTmpSubFolder(WWW_ROOT . $tmpPdfPath, $subFolders);
			unset($folder);
		}		
	}

	private function __delTmpSubFolder($parentPath, $subFolders)
	{
		if (!empty($subFolders)) {
			foreach ($subFolders as $fd) {
				$createTime = filectime($parentPath . $fd);				
				if ($createTime < time()) {
					$delFolder = $parentPath . $fd;
					$folder = new Folder($delFolder);
					$folder->delete($delFolder);
				}
			}
		}
	}
}