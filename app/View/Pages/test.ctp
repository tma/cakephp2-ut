<?php
$checked = $check_value;
echo $this->Form->create('Page');
echo $this->Form->hidden('Page.published_1', ['value' => 1]);
echo $this->Form->input('Page.published_1', ['type' => 'checkbox', 'label' => false, 'value' => 0, 'hiddenField' => false, 'checked' => $checked]);
echo $this->Form->end('Submit');