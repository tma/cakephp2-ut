<?php
class AllControllersTest extends PHPUnit_Framework_TestSuite {

/**
 * suite method, defines tests for this suite.
 *
 * @return void
 */
	public static function suite() {
		$suite = new CakeTestSuite('All Controller related class tests');

		$suite->addTestFile(ROOT . DS . APP_DIR . DS . 'Test' . DS . 'Case' . DS . 'Controller' . DS . 'FacilitiesControllerTest.php');
		return $suite;
	}
}