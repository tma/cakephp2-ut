<?php
class FacilitiesControllerTest extends ControllerTestCase
{
	public function testIndexPagingLimit()
	{
		$result = $this->testAction('/facilities/index', ['return' => 'vars']);
		if (isset($result['facilities'])) {
			$this->assertLessThanOrEqual(1, count($result['facilities']));
		}
		if (isset($result['facilities_deleted'])) {
			$this->assertLessThan(3, count($result['facilities_deleted']));
		}
	}

	public function testIndexFacilityDeleted()
	{
		$result = $this->testAction('/facilities/index', ['return' => 'vars']);
		if (isset($result['facilities_deleted'])) {
			$this->assertTrue($this->__checkIndexDeleted($result['facilities_deleted']));
		}
	}

	public function testAddPostDataError()
	{
		$data = ['Facility' => [
			'facility_name' => 'Test with invalid data',
			'classification_code_id' => null
		]];
		$result = $this->testAction('/facilities/add', ['data' => $data, 'method' => 'post']);
		debug($result);
	}

	public function testAddPostDataValie()
	{
		$data = ['Facility' => [
			'facility_name' => 'Test with valid data',
			'classification_code_id' => 1
		]];
		$result = $this->testAction('/facilities/add', ['data' => $data, 'method' => 'post']);
		debug($result);
	}

	private function __checkIndexDeleted($facilities)
	{
		foreach ($facilities as $value) {
			if ($value['Facility']['deleted'] == 0) {
				return false;
				break;
			}
		}
		return true;
	}
}