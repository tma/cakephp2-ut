<?php
class Article extends AppModel {

	public $actsAs = array(
        'Search.Searchable'
    );

    public $belongsTo = array(
        'User'
    );

    public $filterArgs = array(
        'title' => array(
            'type' => 'like'
        ),
        'status' => array(
            'type' => 'value'
        ),
        'blog_id' => array(
            'type' => 'lookup',
            'formField' => 'blog_input',
            'modelField' => 'title',
            'model' => 'Blog'
        ),
        'search' => array(
            'type' => 'like',
            'field' => 'Article.description'
        ),
        'range' => array(
            'type' => 'expression',
            'method' => 'makeRangeCondition',
            'field' => 'Article.views BETWEEN ? AND ?'
        ),
        'username' => array(
            'type' => 'like', 'field' => array(
                'User.username',
            )
        ),
        'filter' => array(
            'type' => 'query',
            'method' => 'orConditions'
        ),
        'year' => array(
            'type' => 'query',
            'method' => 'yearRange',
            'field' => 'Article.created'
        ),
    );

	// Or conditions with like
    public function orConditions($data = array()) {
        $filter = $data['filter'];
        $condition = array(
            'OR' => array(
                $this->alias . '.title LIKE' => '%' . $filter . '%',
                $this->alias . '.body LIKE' => '%' . $filter . '%',
            )
        );
        return $condition;
    }

    // Turns 2000 - 2014 into a search between these two years
    public function yearRange($data = array()) {
        if (strpos($data['year'], ' - ') !== false){
            $tmp = explode(' - ', $data['year']);
            $tmp[0] = $tmp[0] . '-01-01';
            $tmp[1] = $tmp[1] . '-12-31';
        } else {
        	$tmp[0] = $data['year'] . '-01-01';
            $tmp[1] = $data['year'] . '-12-31';
        }
        return array(
        	$this->alias . '.created >=' => $tmp[0],
        	$this->alias . '.created <=' => $tmp[1]
        );
    }
}