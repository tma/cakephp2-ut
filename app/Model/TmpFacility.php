<?php
App::uses('Facility', 'Model');
class TmpFacility extends Facility {
	/*public $hasOne = [
		'FacilityFee' => ['foreignKey' => false, 'conditions' => ['FacilityFee.facility_id = TmpFacility.facility_id']]
	];*/

	public $hasMany = [
		'FacilityFee' => ['foreignKey' => false, 'conditions' => ['FacilityFee.facility_id = TmpFacility.facility_id']]
	];
}